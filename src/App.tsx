import "./App.css";
import api from "./api";
import { useEffect, useState } from "react";
import { useQuery } from "react-query";
import { Users } from "./api/users";
import { useTranslation } from "react-i18next";
import { PetApi, Pet } from "./api/api";

function App() {
  const { t, i18n } = useTranslation();
  const [getPetById, setGetPetById] = useState<number | string | undefined>(0);
  const [addPet, setAddPet] = useState<number | string | undefined>(0);
  const [updatePetById, setUpdatePetById] = useState<
    number | string | undefined
  >(0);
  const [updatePetName, setUpdatePetName] = useState<string | undefined>("");

  const query = useQuery("users", async () => {
    const response = await api.get<Users>("/users");
    return response.data.data;
  });

  const { data: users, isLoading, error } = query;

  useEffect(() => {
    if (!isLoading) {
      const pets = new PetApi();

      // Add Pet
      pets
        .addPet({
          id: 8103,
        } as Pet)
        .then((message) => {
          if (message.status <= 401) {
            setAddPet(message.data.id);
          }
        })
        .catch((error) => {
          setAddPet(`unable to fetch data`);
        });

      // Get Pet
      pets
        .getPetById(8103)
        .then((message) => {
          if (message.status <= 401) {
            setGetPetById(message.data.id);
          }
        })
        .catch((error) => setGetPetById(`unable to fetch data`));

      // Update Pet
      pets
        .updatePet({
          id: 8102,
          name: "Guru cat",
        } as Pet)
        .then((message) => {
          if (message.status <= 401) {
            setUpdatePetById(message.data.id);
            setUpdatePetName(message.data.name);
          }
        })
        .catch((error) => {
          setUpdatePetById(`unable to fetch data`);
          setUpdatePetName(`unable to fetch data`);
        });

      console.log("Delete deletePet:", pets.deletePet(8102));
    }
  }, [isLoading]);

  if (isLoading) {
    return <p>Loading...</p>;
  }

  if (error) {
    return <p>Error!</p>;
  }

  return (
    <div>
      {!isLoading && !error ? (
        <>
          <div>petAdd: {addPet}</div>
          <div>getPetById: {getPetById}</div>
          <div>
            updatePetById and petName:
            {`${updatePetById}, ${updatePetName}`}
          </div>
        </>
      ) : (
        `Unable to load data`
      )}

      <div>i18next {t("test")}</div>
      <div>Language code: {i18n.language}</div>
      <button onClick={() => i18n.changeLanguage("zh")}>{t("chinese")}</button>
      <button onClick={() => i18n.changeLanguage("en")}>{t("english")}</button>
      {users?.map((user) => {
        return (
          <div key={user.id}>
            <h1>{`${user.first_name} ${user.last_name.toUpperCase()}`}</h1>
            <img src={user.avatar} alt={user.first_name} />
            <p>{user.email}</p>
          </div>
        );
      })}
    </div>
  );
}

export default App;
